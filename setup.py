"""Setup configuration."""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mlmonkey",
    version="0.0.1",
    author="dataroots",
    author_email="info@dataroots.io",
    description="Helpers package for ML framework",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=[
        "scikit-learn==0.20",
        "numpy==1.15",
        "scipy==1.1",
    ],
    extras_require={
        "graphs_tables": ["pandas==0.23.4",],
        "dev": [
            "tox==3.5",
            "pytest==3.7",
            "flake8==3.6",
            "flake8-docstrings==1.3",
            "pytest-cov==2.6",
        ]

    },
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent"
    ]
)
