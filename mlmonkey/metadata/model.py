"""Metadata helper functions."""
import json
import os
import sys
import logging

import sklearn

from .utils import base_metadata

logger = logging.getLogger(__name__)

SCORE_ROOT_KEYS = ('value', 'metric', 'strategy', )


class ModelMetadata(object):
    """Metadata class to hold ML model information."""

    def __init__(self, model_location, model_object, model_description=None,
                 data_location=None, data_identifier=None, feature_names=None,
                 feature_importance=None, testing_strategy=None, scores=None,
                 model_hyperparams=None, extra_metadata=None):
        """
        Generate metadata for provided information.

        More detailed documentation can be found in README.

        :param model_location: Path (including filename) to saved serialized model.
        :param model_object: Model object.
        :param model_description: Textual description of the model (free-form).
        :param data_location: Path to data used for model training.
        :param data_identifier: Uniquely identify data version used for training
        the model. Useful for model reproducibility. Can be None if it is not
        possible to have such identifier.
        :param feature_names: Feature names.
        :param feature_importance: Pairs of (feature name, importance).
        :param testing_strategy: Description of the strategy used for
        testing/evaluating model.
        :param scores: Model evaluation results. Refer to check_scores_structure
        function for more details on valid scores structure.
        :param model_hyperparams: Dictionary (key-value pairs) containing
        hyperparameters used when model was fitted.
        :param extra_metadata: Additional metadata user can provide as a
        dictionary.

        :return: metadata object
        """
        if model_location is None:
            raise Exception('Model location must be provided.')

        if model_object is None:
            raise Exception('Model object must be provided.')

        if feature_names is not None and not isinstance(feature_names, list):
            raise Exception('Feature names must be a list.')

        if scores is None:
            scores = {}
        self._check_scores_structure(scores)

        model_type = '{}.{}'.format(model_object.__module__,
                                    model_object.__class__.__name__)

        # TODO: this can probably be removed if we default to git versioning > discuss
        # model_id = hashlib.sha1(
        #     str(get_git_commit()).encode('utf-8') +
        #     str(datetime.datetime.now()).encode('utf-8')) \
        #     .hexdigest()

        if feature_names is not None:
            num_features = len(feature_names)
        else:
            num_features = None

        if model_hyperparams is None:
            if isinstance(model_object, sklearn.base.BaseEstimator):
                model_hyperparams = model_object.get_params()

        model_object_size = '{} bytes'.format(sys.getsizeof(model_object))

        metadata = {
            'model_location': model_location,
            'model_description': model_description,
            'model_type': model_type,
            'model_hyperparams': model_hyperparams,
            'model_size': model_object_size,
            'input_data_location': data_location,
            'input_data_identifier': data_identifier,
            'num_features': num_features,
            'feature_names': feature_names,
            'feature_importance': feature_importance,
            'testing_strategy': testing_strategy,
            'scores': scores
        }

        if extra_metadata is not None:
            metadata.update(extra_metadata)

        # TODO: this could have unwanted side effects: fix
        # bleh to backwards compat.. we need py3!
        metadata.update(base_metadata())

        logger.info('Model metadata generated:\n{}'.format(
            json.dumps(metadata, indent=4)))

        self.metadata = metadata

    def add_score(self, metric, strategy, value):
        """Dynamically add a score to the metadata object.

        :param self: Class instance.
        :param metric: The name of the metric
        :param strategy: The strategy used to obtain the score
        :param value: The value (or array of values) of the score.
        """
        if not self.metadata['scores'].get(metric):
            self.metadata['scores'][metric] = {strategy: value}
        else:
            self.metadata['scores'][metric][strategy] = value

    def get(self):
        """Return metadata information.

        :param self: Class instance

        :return: dictionary object
        """
        return self.metadata

    def __repr__(self):
        """Show metadata as indented json dump."""
        return json.dumps(self.metadata, indent=4)

    def __getitem__(self, item):
        """Access metadata keys/values."""
        return self.metadata.get(item)

    def __setitem__(self, key, value):
        """Set metadata key/values."""
        self.metadata[key] = value

    def save_to_file(self, dirname, filename=None):
        """Save metadata to file.

        :param self: Class instance.
        :param dirname: The directory to store the metadata.
        :param filename: Filename of the metadata (without extension). If not  given will use the model (file)name.

        :return: None
        """
        if filename is None:
            base = os.path.basename(self.metadata['model_location'])
            model_filename = os.path.splitext(base)[0]
            pathname = os.path.join(dirname, '{}-metadata.json'.format(model_filename))
        else:
            pathname = os.path.join(dirname, filename)

        with open(pathname, 'w') as f:
            json.dump(self.metadata, f, indent=4)

        logger.info('Metadata stored at {}'.format(pathname))

    @classmethod
    def load_from_file(cls, pathname):
        """Load metadata from file.

        :param self: Class instance.
        :param pathname: Full path to the metadata file.
        """
        with open(pathname, 'r') as f:
            metadata = json.load(f)

        class_instance = cls.__new__(cls)
        class_instance.metadata = metadata

        return class_instance

    @staticmethod
    def _check_scores_structure(scores):
        """
        Check if scores object complies with predefined structure.

        :param scores: A list of dictionaries. Each dictionary should
        contain on the keys in `SCORE_ROOT_KEYS`.
        :return: None
        """
        if not isinstance(scores, dict):
            raise TypeError('scores object should be a dictionary')

        for metric, score in scores.items():
            if not isinstance(score, dict):
                raise TypeError('score object should be a dictionary')
