"""Prediction metadata logic."""

import logging
import json
import os

from .utils import base_metadata

logger = logging.getLogger(__name__)


class PredictionMetadata(object):
    """Metadata class to hold ML prediction information."""

    def __init__(self, model_location, input_identifier, output_identifier,
                 extra_metadata=None):
        """
        Generate metadata for prediction purposes.

        :param model_location: pathname to model
        :param input_identifier: A (serializable) object that identifies the input data. For single observation
        or smallish batch predictions this can be the full input array.
        :param output_identifier: A (serializable) object that identifies the output values. For single predictions
        or smallish batch predictions this can be the full output array.

        :return: None
        """
        self.metadata = dict(model_location=model_location,
                             input_identifier=input_identifier,
                             output_identifier=output_identifier)

        self.metadata.update(base_metadata())

        if extra_metadata:
            self.metadata['extra_metadata'] = extra_metadata

        logger.info('Prediction metadata generated:\n{}'.format(
            json.dumps(self.metadata, indent=4)))

    def get(self):
        """Return the metadata as a dictionary."""
        return self.metadata

    def __getitem__(self, item):
        """Access metadata keys/values."""
        return self.metadata.get(item)

    def __repr__(self):
        """Show metadata as indented json dump."""
        return json.dumps(self.metadata, indent=4)

    def save_to_file(self, dirname, filename=None):
        """Save metadata to file.

        :param self: Class instance.
        :param dirname: The directory to store the metadata.
        :param filename: Filename of the metadata (without extension). If not  given will use the model (file)name.

        :return: None
        """
        if filename is None:
            filename = os.path.basename(self.metadata['model_location'])
        pathname = os.path.join(dirname, '{}.metadata.json'.format(filename))

        with open(pathname, 'w') as f:
            json.dump(self.metadata, f, indent=4)

        logger.info('Metadata stored at {}'.format(pathname))

    @classmethod
    def load_from_file(cls, pathname):
        """Load metadata from file.

        :param self: Class instance.
        :param pathname: Full path to the metadata file.
        """
        with open(pathname, 'r') as f:
            metadata = json.load(f)

        class_instance = cls.__new__(cls)
        class_instance.metadata = metadata

        return class_instance
