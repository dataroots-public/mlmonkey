"""Metadata utility functions."""

import subprocess
import sys
import datetime


def get_git_commit():
    """Get git commit if it exists.

    :return: git commit string or 'na'
    """
    try:
        return subprocess.check_output(
            ['git', 'rev-parse', 'HEAD']).decode(sys.stdout.encoding).strip()

    except subprocess.CalledProcessError:
        return 'no-git-commit-available'


def base_metadata():
    """
    Generate basic metadata.

    Includes git commit hash and a timestamp.
    BTW, subprocess.check_output is used for backwards compatibility.

    :return: metadata dictionary
    """
    return {'git_commit': get_git_commit(),
            'timestamp': str(datetime.datetime.now())}
