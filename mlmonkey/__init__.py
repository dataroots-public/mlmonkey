"""ML monkey."""

import logging
import os

logging.basicConfig(level=os.getenv('LOG_LEVEL', 'WARNING'))

logging \
    .getLogger(__name__) \
    .addHandler(logging.NullHandler())
