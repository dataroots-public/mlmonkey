"""Helper methods to generate tables for reports."""

try:
    import pandas as pd
except ImportError:
    import warnings
    warnings.warn('Table dependencies not found please install via \'mlmonkey[graphs_tables]\' to enable this feature')

from mlmonkey.metadata import ModelMetadata


def extract_table_data(metadata, metrics_to_exclude=None,
                       strategies_to_exclude=None):
    """
    Extract scores data in tabular format.

    :param metadata: Model metadata object
    :param metrics_to_exclude: list of metrics to exclude
    :param strategies_to_exclude: list of strategies to exclude
    """
    if not isinstance(metadata, ModelMetadata):
        raise Exception('Invalid metadata format.')

    scores = metadata['scores']

    all_metrics = set()
    all_strategies = set()

    for metric, strategy_scores in scores.items():
        if not isinstance(strategy_scores, dict):
            raise Exception('Invalid scores structure.')
        for (strategy, score) in strategy_scores.items():
            if not isinstance(score, list) and not isinstance(score, dict):
                all_metrics.update([metric])
                all_strategies.update([strategy])

    if metrics_to_exclude is not None:
        all_metrics = [m for m in all_metrics if m not in metrics_to_exclude]
    if strategies_to_exclude is not None:
        all_strategies = [s for s in all_strategies if s not in strategies_to_exclude]

    dataframe = pd.DataFrame(columns=all_strategies, index=all_metrics)

    for metric, strategy_score in scores.items():
        if metric in all_metrics:
            for strategy_name, score in strategy_score.items():
                if strategy_name in all_strategies:
                    dataframe.loc[metric][strategy_name] = score

    return dataframe
