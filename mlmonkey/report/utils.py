"""Utilities for reporting purposes."""

import functools
from ..metadata.model import ModelMetadata
import copy


def metadata_mapper(**kwargs):
    """Map function arguments to metadata key/values."""
    annotation_keyword_args = kwargs

    def _metadata_mapper(func):
        @functools.wraps(func)
        def wrapper_metadata_mapper(*args, **kwargs):
            mapping = copy.deepcopy(annotation_keyword_args)
            # We accept both ModelMetadata object and dictionary, as a first argument.
            if len(args) > 0 and isinstance(args[0], ModelMetadata):
                metadata = args[0].get()
                args = args[1:]
                for k, v in mapping.items():
                    try:
                        value = functools.reduce(lambda dict, key: dict[key], v.split('|'), metadata)
                        mapping[k] = value
                    except Exception as e:
                        # comprehensive py2/py3 compat way is tedious - keeping it simple here (PEP 353)
                        raise KeyError('mapping \'{}\' cannot be resolved in metadata - {}'.format(v, str(e)))
                mapping.update(kwargs)
                return func(*args, **mapping)
            else:
                return func(*args, **kwargs)

        return wrapper_metadata_mapper
    return _metadata_mapper
