"""Helper methods to generate data for reporting graphs."""
from ..metadata import ModelMetadata
from .utils import metadata_mapper


@metadata_mapper(plot_data='scores|roc_curve')
def extract_roc_data(plot_data, strategies=None):
    """Extract data for ROC curves.

    :param plot_data: ModelMetadata object or dictionary containing ROC data (see examples).
    :param strategies: Strategies to retain.
    :return: List of data for all ROC curves. Data for single curve is a dictionary with keys: 'fpr', 'tpr', 'name',
    where name represents curve name.

    The `plot_data` can contain these info (examples):

    .. code-block:: python

        {'holdout': {'tpr': [tpr_curve_data, ...], 'fpr': [fpr_curve_data, ...], 'auroc': auroc_value}}

    It is possible to plot multiple curves per validation strategy. For example for 2-fold CV you do
    the following:

    .. code-block:: python

        {'cv_fold': [{'tpr': [tpr_curve_data, ...], 'fpr': [fpr_curve_data, ...], 'auroc': auroc_value},
                 {'tpr': [tpr_curve_data, ...], 'fpr': [fpr_curve_data, ...], 'auroc': auroc_value}]}

    A full example:

    .. code-block:: python

        m = ModelMetadata(model_location='m.l', model_description='m.d',
                      model_object=RandomForestClassifier(), data_location='d.loc',
                      data_identifier=None, feature_names=None,
                      testing_strategy='ts', scores=None,
                      model_hyperparams=None, extra_metadata=None)

        fpr = [0., 0., 0.5, 0.5, 1.]
        fpr2 = [0., 0., 0.3, 0.5, 1.]
        tpr = [0., 0.5, 0.5, .9, 1.]
        tpr2 = [0., 0.5, 0.3, .9, 1.]
        auroc = .8

        m.add_score('roc_curve', 'holdout', {'fpr': fpr, 'tpr': tpr, 'auroc': auroc})
        m.add_score('roc_curve', 'cv_fold', [{'fpr': fpr, 'tpr': tpr, 'auroc': auroc},
                    {'fpr': fpr2, 'tpr': tpr2, 'auroc': auroc}])

        plot_auc(m)

    """
    data = []
    if strategies is not None and not isinstance(strategies, list):
        strategies = [strategies]  # leave the option for single-value argument 'strategies'

    for strategy, curve_data in plot_data.items():

        if strategies is not None and strategy not in strategies:
            continue

        # allow for multiple curve data per strategy (e.g. kfold validation curves)
        if not isinstance(curve_data, list):
            curve_data = [curve_data]

        for i, curve in enumerate(curve_data):
            trace = {
                'fpr': curve['fpr'],
                'tpr': curve['tpr'],
                'name': '{} - {}'.format(strategy, i + 1)
            }
            data.append(trace)

    return data


@metadata_mapper(plot_data='scores|pr_curve')
def extract_pr_data(plot_data, strategies=None):
    """Extract data for PR curves.

    :param plot_data: ModelMetadata object or dictionary containing precision and recall data (see examples).
    :param strategies: Strategies to retain.
    :return: List of data for all PR curves. Data for single curve is a dictionary with keys: 'recall', 'precision',
    'name', where name represents curve name.

    The `plot_data` can contain these info (examples):

    .. code-block:: python

        {'cv': {'precision': [precision1, precision2, ...], 'recall': [recall1, recall2, ...]}}

    It is possible to plot multiple curves per validation strategy. For example for 2-fold CV you do
    the following:

    .. code-block:: python

        {'cv_fold': [{'precision': [precision1, precision2, ...], 'recall': [recall1, recall2, ...]},
                 {'precision': [precision1, precision2, ...], 'recall': [recall1, recall2, ...]}]}

    A full example:

    .. code-block:: python

        m = ModelMetadata(model_location='m.l', model_description='m.d',
                      model_object=RandomForestClassifier(), data_location='d.loc',
                      data_identifier=None, feature_names=None,
                      testing_strategy='ts', scores=None,
                      model_hyperparams=None, extra_metadata=None)

        precision = [0.2, 0.35, 0.54, 0.58, 1.]
        recall = [1., 0.8, 0.62, 0.59, 0.]
        precision2 = [0.25, 0.45, 0.57, 0.58, 1.]
        recall2 = [1., 0.85, 0.62, 0.62, 0.]

        m.add_score('pr_curve', 'holdout', {'precision': precision, 'recall': recall})
        m.add_score('pr_curve', 'cv_fold', [{'precision': precision, 'recall': recall},
                                        {'precision': precision2, 'recall': recall2}])

        extract_pr_data(m)
    """
    data = []
    if strategies is not None and not isinstance(strategies, list):
        strategies = [strategies]  # leave the option for single-value argument 'strategies'

    for strategy, curve_data in plot_data.items():

        if strategies is not None and strategy not in strategies:
            continue

        # allow for multiple curve data per strategy (e.g. kfold validation curves)
        if not isinstance(curve_data, list):
            curve_data = [curve_data]

        for i, curve in enumerate(curve_data):
            trace = {
                'recall': curve['recall'],
                'precision': curve['precision'],
                'name': '{} - {}'.format(strategy, i+1)
            }
            data.append(trace)

    return data


@metadata_mapper(plot_data='actual_and_predicted')
def extract_actual_and_predicted(plot_data):
    """Return actual and predicted values.

    :param plot_data: ModelMetadata object or dictionary containing actual_and_predicted
    :return: Dictionary containing actual and predicted values.
    They can be accessed using keys 'actual' and 'predicted'.
    """
    return plot_data


def extract_feature_importance(metadata):
    """Extract feature importance data.

    :param metadata: Original metadata
    :return: Dictionary containing feature names and corresponding scores.
    They can be accessed using keys 'feature_names' and 'feature_scores'.
    """
    if not isinstance(metadata, ModelMetadata):
        raise Exception('Invalid metadata format.')

    feature_importance = metadata['feature_importance']

    feature_names = []
    feature_scores = []
    for fi in feature_importance:
        feature_names.append(fi[0])
        feature_scores.append(fi[1])

    feature_names.reverse()
    feature_scores.reverse()

    return {
        'feature_names': feature_names,
        'feature_scores': feature_scores
    }
