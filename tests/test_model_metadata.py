"""Test generation of metadata file."""

import sys
import os
import tempfile

import pytest
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification

from mlmonkey.metadata import ModelMetadata

X, y = make_classification(n_samples=1000, n_features=4,
                           random_state=0)
clf = RandomForestClassifier(n_estimators=100, max_depth=2,
                             random_state=0)
clf.fit(X, y)


default_metadata_params = dict(model_location='m.l', model_description='m.d',
                          model_object=clf, data_location='d.loc',
                          data_identifier=None, feature_names=None,
                          testing_strategy='ts', scores=None,
                          model_hyperparams=None, extra_metadata=None)

def test_valid_information():
    """Test case when we provide all and valid data for
    metadata generation."""

    model_location = 'Some model location'
    description = 'Some model description'
    data_location = '/here/or/there'
    data_identifier = 12
    feature_names = ['feature1', 'feature2']
    testing_strategy = 'Some testing strategy'

    scores = {
        'precision': {
            'cv': .6
        },
        'recall': {
            'cv': .4
        }
    }
    metadata = ModelMetadata(model_location=model_location,
                                 model_description=description,
                                 model_object=clf,
                                 data_location=data_location,
                                 data_identifier=data_identifier,
                                 feature_names=feature_names,
                                 testing_strategy=testing_strategy,
                                 scores=scores,
                                 model_hyperparams=None,
                                 extra_metadata=None).get()

    assert metadata['model_location'] == model_location
    assert metadata['model_description'] == description
    assert metadata['model_type'] == \
           'sklearn.ensemble.forest.RandomForestClassifier'
    assert metadata['model_hyperparams']['n_estimators'] == 100
    assert metadata['model_hyperparams']['max_depth'] == 2
    assert metadata['model_hyperparams']['random_state'] == 0
    assert metadata['model_size'] == '{} bytes'.format(sys.getsizeof(clf))
    assert metadata['input_data_location'] == data_location
    assert metadata['input_data_identifier'] == data_identifier
    assert metadata['num_features'] == 2
    assert metadata['feature_names'] == feature_names
    assert metadata['testing_strategy'] == testing_strategy
    assert metadata['scores'] == scores
    assert metadata['git_commit'] is not None
    assert metadata['timestamp'] is not None


def test_hyperparameters():
    """Test if hyperparameters are processed correctly."""

    hypers = {
        'hyper1': 'value1',
        'hyper2': 2
    }
    # Check if hyperparameters are overwriten
    metadata = generate_with_default(model_hyperparams=hypers,
                                     model_object=clf)
    assert metadata['model_hyperparams']['hyper1'] == 'value1'
    assert metadata['model_hyperparams']['hyper2'] == 2
    assert len(metadata['model_hyperparams']) == 2

    # Check if sklearn hyperparameters are being used as fallback
    metadata = generate_with_default(model_hyperparams=None,
                                     model_object=clf)
    assert metadata['model_hyperparams']['n_estimators'] == 100
    assert metadata['model_hyperparams']['max_depth'] == 2
    assert metadata['model_hyperparams']['random_state'] == 0


def test_extrametadata():
    """Test if extra metadata is being add to core metadata."""

    extra_metadata = {
        'meta1': 'value1',
        'meta2': 2
    }

    # Check that extra metadata are added
    metadata = generate_with_default(extra_metadata=extra_metadata)
    assert metadata['meta1'] == 'value1'
    assert metadata['meta2'] == 2


def test_dynamic_score_add():
    m = generate_with_default()
    val = [1, 2, 3]
    m.add_score('auc_roc', 'cv', val)
    m.add_score('blaa', 'cv', val)
    m.add_score('blaa', 'pr', val)
    assert m.get()['scores']['auc_roc']['cv'] == val
    assert m.get()['scores']['blaa']['cv'] == val
    assert m.get()['scores']['blaa']['pr'] == val


def test_dynamic_metadata_add():
    m = generate_with_default()
    assert m['input_data_identifier'] is None
    m['input_data_identifier'] = 1
    assert m['input_data_identifier'] == 1
    m['input_data_location'] = 'loc'
    assert m['input_data_location'] == 'loc'


def test_missing_model_location():
    with pytest.raises(Exception, match='Model location must be provided.'):
        generate_with_default(model_location=None)


def test_missing_model_object():
    with pytest.raises(Exception, match='Model object must be provided.'):
        generate_with_default(model_object=None)


def test_features_names_format():
    with pytest.raises(Exception, match='Feature names must be a list.'):
        generate_with_default(feature_names='name1')


def test_save_load_metadata_to_file():
    tmp_dir = tempfile.gettempdir()
    m = ModelMetadata(**default_metadata_params)
    m.save_to_file(tmp_dir)
    base = os.path.basename(default_metadata_params['model_location'])
    model_filename = os.path.splitext(base)[0]
    pathname = os.path.join(tmp_dir, '{}-metadata.json'.format(model_filename))

    assert os.path.isfile(pathname)

    m2 = ModelMetadata.load_from_file(pathname)
    assert m2['model_location'] == m['model_location']


def generate_with_default(model_location='m.l', model_description='m.d',
                          model_object=clf, data_location='d.loc',
                          feature_names=None, testing_strategy='ts',
                          model_hyperparams=None, extra_metadata=None):
    """Call generation of metadata with provided valid defaults."""

    return ModelMetadata(model_location=model_location,
                             model_description=model_description,
                             model_object=model_object,
                             data_location=data_location,
                             feature_names=feature_names,
                             testing_strategy=testing_strategy,
                             model_hyperparams=model_hyperparams,
                             extra_metadata=extra_metadata)



def test_error_raised():
    # should all raise an exception
    with pytest.raises(TypeError):
        ModelMetadata._check_scores_structure([{'value': .6, 'strategy': 'holdout', 'metric': 'auroc', 'bla': 3}])

    with pytest.raises(TypeError):
        ModelMetadata._check_scores_structure({'precision': [.8]})

    with pytest.raises(TypeError):
        ModelMetadata._check_scores_structure(None)

    with pytest.raises(TypeError):
        ModelMetadata._check_scores_structure([1,2,3])


def test_correct_usage():
    # should pass
    ModelMetadata._check_scores_structure({'precision': {'cv': .99}})