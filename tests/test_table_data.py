"""Test methods for extracting table data, for report."""
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier

from mlmonkey.metadata import ModelMetadata
from mlmonkey.report import extract_table_data

def test_regression_table():
    """Test whether data for regression table is retrieved."""

    measures = {
        "r2": {
            "cv":[
                0.935, 0.925, 0.91, 0.934, 0.945
            ],
            "cv mean": 0.932,
            "hold-out": 0.958
        },
        "mse": {
            "cv":[
                0.735, 0.725, 0.71, 0.734, 0.745
            ],
            "cv mean": 0.732,
            "hold-out": 0.758
        }
    }

    metadata = ModelMetadata(model_location='m.l', model_description='m.d',
                            model_object=RandomForestRegressor(), data_location='d.loc',
                            data_identifier=None, feature_names=None,
                            testing_strategy='ts', scores=measures)

    df = extract_table_data(metadata)
    assert sorted(df.columns.values.tolist()) \
           == sorted(['cv mean', 'hold-out'])
    assert sorted(list(df.index)) == sorted(['r2', 'mse'])
    assert df.loc['r2']['cv mean'] == 0.932

    df = extract_table_data(metadata, metrics_to_exclude=['mse'])
    assert sorted(df.columns.values.tolist()) \
           == sorted(['cv mean', 'hold-out'])
    assert sorted(list(df.index)) == sorted(['r2'])
    assert df.loc['r2']['hold-out'] == 0.958


def test_classification_table():
    """Test whether data for classification table is retrieved."""

    measures = {
        "precision": {
            "cv": [0.5, 0.46, 0.48],
            "cv mean": 0.48,
            "hold-out": 0.49
        },
        "recall": {
            "cv": [0.7, 0.76, 0.78],
            "cv mean": 0.74,
            "hold-out": 0.75
        },
        "pr_curve": {
            "cv_fold": [
                {'precision':[0.25, 0.43, 0.63], 'recall':[0.93, 0.71, 0.52]},
                {'precision': [0.25, 0.43, 0.63], 'recall': [0.93, 0.71, 0.52]},
                {'precision': [0.25, 0.43, 0.63], 'recall': [0.93, 0.71, 0.52]},
            ],
            "cv mean": {'precision':[0.25, 0.43, 0.63], 'recall':[0.93, 0.71, 0.52]},
            "hold-out": {'precision':[0.25, 0.43, 0.63], 'recall':[0.93, 0.71, 0.52]}
        }
    }

    metadata = ModelMetadata(model_location='m.l', model_description='m.d',
                             model_object=RandomForestClassifier(), data_location='d.loc',
                             data_identifier=None, feature_names=None,
                             testing_strategy='ts', scores=measures)

    df = extract_table_data(metadata)
    assert sorted(df.columns.values.tolist()) \
           == sorted(['cv mean', 'hold-out'])
    assert sorted(list(df.index)) == sorted(['precision', 'recall'])
    assert df.loc['precision']['hold-out'] == 0.49
