import os
import tempfile

from mlmonkey.metadata import PredictionMetadata

def test_create_prediction_metadata():
    pm = PredictionMetadata('/here/or/there', 'my_input_id', 'my_output_id', extra_metadata={'bla':'blob'})

    pm.get()

def test_accessing_metadata_attrs():
    pm = PredictionMetadata('/here/or/there', 'my_input_id', 'my_output_id', extra_metadata={'bla': 'blob'})
    assert pm['model_location'] == '/here/or/there'

def test_save_load_prediction_metadata_to_file():
    tmp_dir = tempfile.gettempdir()
    model_location = os.path.join(tmp_dir, 'my_model.p')
    pm = PredictionMetadata(model_location, 'my_input_id', 'my_output_id', extra_metadata={'bla': 'blob'})

    pm.save_to_file(tmp_dir)
    pathname = os.path.join(tmp_dir, '{}.metadata.json'.format(
        os.path.basename(model_location)))

    assert os.path.isfile(pathname)

    pm2 = PredictionMetadata.load_from_file(pathname)
    assert pm2['model_location'] == pm['model_location']