"""Test report utils."""

import pytest
from sklearn.ensemble import RandomForestRegressor

from mlmonkey.metadata import ModelMetadata
from mlmonkey.report.utils import metadata_mapper

metadata_regression = ModelMetadata(model_location='m.l', model_description='m.d',
                                    model_object=RandomForestRegressor(), data_location='d.loc',
                                    data_identifier=None, feature_names=None,
                                    testing_strategy='ts', scores={
        'r2': {'cv': 0.9651066561780983},
        'mean_squared_error': {'cv': 0.10383451882024415}
    },
                                    model_hyperparams=None, extra_metadata=None)


def test_mapper():
    @metadata_mapper(r2='scores|r2|cv', mean_squared_error='scores|mean_squared_error|cv',
                     testing_strategy='testing_strategy')
    def f(r2, mean_squared_error, testing_strategy):
        assert r2 == metadata_regression['scores']['r2']['cv']
        assert mean_squared_error == metadata_regression['scores']['mean_squared_error']['cv']
        assert testing_strategy == metadata_regression['testing_strategy']

    @metadata_mapper(r2='scores|r2|cv', mean_squared_error='scores|mean_squared_error|cv',
                     testing_strategy='testing_strategy')
    def f2(r2, mean_squared_error, testing_strategy):
        assert r2 == 1
        assert mean_squared_error == 2
        assert testing_strategy == 3

    f(metadata_regression)
    f2(r2=1, mean_squared_error=2, testing_strategy=3)


def test_error_gracefully():
    @metadata_mapper(r2='scores|r2|cv', mean_squared_error='scores|mean_squared_error|cv3',
                     testing_strategy='testing_strategy')
    def f(r2, mean_squared_error, testing_strategy):
        assert r2 == metadata_regression['scores']['r2']['cv']
        assert mean_squared_error == metadata_regression['scores']['mean_squared_error']['cv']
        assert testing_strategy == metadata_regression['testing_strategy']

    with pytest.raises(KeyError):
        f(metadata_regression)
