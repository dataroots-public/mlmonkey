# mlmonkey

Docs: https://dataroots-public.gitlab.io/mlmonkey/

This package provides helper methods for creating metadata of ML models, deploying Flask endpoint,  
and generating visualizations in model report.

## Generating metadata

Metadata is being generated using helper function that accepts list of arguments as follows:
1. `model_location`: Path to saved serialized model.
2. `model_description`: Textual description of the model (free-form).
3. `model_object`: Model object. Used only for automatic extraction of additional metadata (e.g model type, size etc).
4. `data_location`: Path to data used for model training.
5. `data_identifier`: In case we want to reproduce model, it is important to have version of the data used for training, if possible.  
Data identifier should be any value that can help us to get right version of the data.  
For example, that can include id of the last row in dataset, if that dataset is always updated by appending new rows.
6. `feature_names`: Feature names.
7. `testing_strategy`: Description of the strategy used for testing/evaluating model, i.e. calculating `scores`.  
It should be detailed so the scores can be reproduced. For example, include information if you used cross validation and/or holdout set  
and what setup (number of folds, stratification etc).
8. `scores`: Model evaluation results. These scores should be formatted as map containing one or more metrics:  
`metric name: scores map`, where scores map can contain multiple key-value pairs of type: `strategy name: score value`.  
There are constraints on what can be valid metric name, and what can be strategy name.  
Valid metric names: Those listed in `sklearn.metrics.SCORERS.keys()` +   
`['log_loss', 'mean_absolute_error', 'mean_squared_error', 'mean_squared_log_error', 'median_absolute_error']`  
In addition, if you want to use some alternative metric, you can include it but you must set its name to start with 'custom'.  
Valid names for testing strategy: `['cross_val', 'hold_out']`, or other strategy which name must start with 'custom'.
9. `model_hyperparameters`: Map (key-value pairs) containing hyperparameters used when model was fitted. If not provided,  
and if the `model object` is of sklearn type, these hyper-parameters are extracted automatically using sklearn method.
10. `extra_metadata`: Additional metadata user can provide. These metadata can be for example:
- data type (e.g. csv), which can not be easily extracted automatically since we can have many formats, including distributed datasets, 
- training time on given samples - which might depend whether it's about training on whole dataset, cv,  
which hardware was used etc. Some of these info might be automatically extracted. For now, free-text description might be enough.
- number of data rows (calculation can depend of the type of input data).
- size of data object.


Based on these 10 provided arguments, some additional metadata is automatically extracted:
1. `model_identifier`: Unique ID of the model. Calculated as hash of concatenated git commit number and timestamp.
2. `model_type`: Name of the Python class for the model instance. Eg: sklearn.linear_model.base.LinearRegression.
3. `model_size`: Size of model object (when loaded in RAM) in bytes.
4. `num_features`: Number of features.
5. `git_commit`: Number of git commit associated with the code version used when model was trained.
6. `timestamp`: Time when metadata was generated.
 
We don't keep information about feature engineering in metadata, but rather in the reports.


## Reporting

Reporting is done using jupyter notebooks. The package provides several methods to easily  
generate tables and graphs from existing model metrics. General schema for score data is:  
```
{
    metric_1: {
            evaluation_1: score(s),
            evaluation_2: score(s),
            ...
        },
        
    metric_2: {
            evaluation_1: score(s),
            evaluation_2: score(s),
            ...
        }
}
```
`evaluation` tells how evaluation was performed, e.g. whether it was cross val, cross val fold, holdout etc.   
(In case of cross validation, we can provide scores for each fold separately, or as a list.)  
Scores can represent single values, or more complex structure (object with precision and recall arrays), for example).      
Example for classification: 
```
{
    "precision": {
        "cv": 0.53,
        "cv_fold": [0.52, 0.54, 0.55, 0.48, 0.56],
        "holdout": 0.5
    },
    "recall": {
        ...
    },
    "pr_curve": {
        "cv":{
            'precision': [0.2, 0.63, 0.73, 1.]
            'recall': [1., 0.54, 0.15, 0.]
        },
        "cv_fold": {
            [
                {'precision': [0.25, 0.63, 0.78, 1.], 'recall': [1., 0.57, 0.25, 0.]},
                {'precision': [0.22, 0.65, 0.73, 1.], 'recall': [1., 0.53, 0.12, 0.]}
            ],
        "hold-out": {
            'precision': [0.23, 0.63, 0.75, 1.]
            'recall': [1., 0.55, 0.15, 0.]
        }
    }
}
```  
Example for regression: 
```
{
    "r2": {
        "cv": 0.932,
        "cv_fold": [0.935, 0.925, 0.91, 0.934, 0.945],
        "holdout": 0.958
    },
    ...
}
```

Within this project there are helper methods to visualize calculated metrics:  
All singular scores (not arrays or objects) can be shown in a summary table.  
Also, methods for plotting standard curves are provided (e.g. PR and ROC curves).  

Beside scores, visualizations can also be rendered for some special cases, such as plotting predicted vs. actual values  
for regression models. For this, when generating metadata, within `extra_metadata` field we should provide:  
```
"actual_and_predicted":{
    "actual": [actual1, actual2, ...],
    "predicted": [predicted1, predicted2, ...]
}

```