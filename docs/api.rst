API reference
====================================

.. automodule:: mlmonkey.metadata
    :members:

.. automodule:: mlmonkey.report
    :members:

.. automodule:: mlmonkey.report.graphs
    :members:
