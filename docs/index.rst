mlmonkey - let's go bananas!
============================

.. image:: https://i.giphy.com/media/pFwRzOLfuGHok/giphy.webp

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
